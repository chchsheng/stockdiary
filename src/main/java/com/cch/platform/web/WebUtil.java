package com.cch.platform.web;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.springframework.context.ApplicationContext;
import org.springframework.ui.ModelMap;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import com.cch.platform.base.bean.BaseUser;

public class WebUtil {
	
	private static ThreadLocal<BaseUser> threaduser=new ThreadLocal<BaseUser>();
	
	public static void setUser(BaseUser user){
		threaduser.set(user);
	}
	
	public static BaseUser getUser(){
		return threaduser.get();
	}
	
	
	@SuppressWarnings("unchecked")
	public static Map<String,Object> getParam(ServletRequest request){
		Map<String,Object> result=new HashMap<String,Object>();
		Enumeration<String> names =request.getParameterNames();
		while(names.hasMoreElements()){
			String name=names.nextElement();
//			switch (name) {
//				case "sort": 
//				case "order":result.put("_"+name, request.getParameter(name));break;
//				case "page": result.put("_pageNo", request.getParameter(name));break;
//				case "rows": result.put("_pageSize", request.getParameter(name));break;
//				default:result.put(name, request.getParameter(name));break;
//			}
			if("sort".equals(name)||"sort".equals(name)) {
				result.put("_"+name, request.getParameter(name));
			} else if("page".equals(name)) {
				result.put("_pageNo", request.getParameter(name));
			} else if("rows".equals(name)) {
				result.put("_pageSize", request.getParameter(name));
			} else{
				result.put(name, request.getParameter(name));
			}
		}
		return result;
	}
	
	public static ModelAndView errorView(String msg){
		ModelMap mm=new ModelMap();
		mm.put("sucesse", false);
		mm.put("message", msg);
		return new ModelAndView("jsonView", mm);
	}
	
	public static ModelAndView sucesseView(String msg){
		ModelMap mm=new ModelMap();
		mm.put("sucesse", true);
		mm.put("message", msg);
		return new ModelAndView("jsonView", mm);
	}

}
