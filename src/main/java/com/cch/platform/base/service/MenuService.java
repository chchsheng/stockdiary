package com.cch.platform.base.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cch.platform.base.bean.BaseMenu;
import com.cch.platform.base.bean.BaseRole;
import com.cch.platform.base.bean.BaseUser;
import com.cch.platform.service.BaseService;

@Service
public class MenuService extends BaseService<BaseMenu>{
	
	/**
	 * 根据用户角色，查询菜单
	 * @param user
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<BaseMenu> getAllMenu(BaseUser user){
		BaseRole br=null;
		List<BaseMenu> re=null;
		if(user.getRoleId()!=null){
			br=beanDao.load(BaseRole.class, user.getRoleId());
		}
		if(br==null){
			return re;
		}
		
		if("sa".equals(br.getRoleCode())){
			re=beanDao.loadAll(BaseMenu.class);
		}else{
			re=beanDao.find("from BaseMenu where code in " +
					"(select menuCode from BaseRolemenu where roleCode=?)",
					br.getRoleCode());
		}
		return re;
	}
}
