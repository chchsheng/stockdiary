package com.cch.platform.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.cch.platform.dao.HibernateDao;

@Service
public class CommonService {
	
	@Resource(name="hibernateDao")
	protected HibernateDao beanDao;
	
	public <T> T get(Class<T> entityClass ,Serializable id) {
		return beanDao.get(entityClass,id);
	}
	
	public <T>T get(Class<T> entityClass,Map<String, Object> param) {
		return beanDao.get(entityClass,param);
	}
	
	public void save(Object entity){
		beanDao.save(entity);
	}

	public void update(Object entity) {
		beanDao.update(entity);
	}
	
	public void saveOrUpdate(Object entity){
		beanDao.saveOrUpdate(entity);
	}
	
	public void delete(Object entity) {
		beanDao.delete(entity);
	}
	
	public <T> Map pageQuery(Class<T> entityClass,Map<String, Object> param) {
		return beanDao.pageQuery(entityClass,param);
	}
	
	public <T>List<T> find(Class<T> entityClass,Map<String, Object> params){
		return beanDao.find(entityClass,params);
	}
}
