<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<jsp:include page="/core/tool/resources.jsp"></jsp:include>
	<script src="./getstockprice.js"></script>

</head>

<body class="easyui-layout">
	<div data-options="region:'west'" style="width:400px;">
		<div id="toolbar" style="margin:5px">
			<div>
				Date From: <input id='dateFrom' class="easyui-datebox" style="width:130px"/>
				To: <input id='dateTo' class="easyui-datebox" style="width:130px"/>
			</div>
			<div style="text-align:right;margin:5px">
	       		<a class="easyui-linkbutton" iconCls="icon-search" onclick="getPrice()">同步</a>
			</div>
		</div>
		<table id="dg"  class='easyui-edatagrid' toolbar="#toolbar" style="width:100%" 
			data-options="title:'股票',rownumbers:true,singleSelect:true,pagination:true,method:'post',idField:'stockCode',editing:false,
				url : plat.fullUrl('/stock/stockset/pagequery.do'),onSelect:select_stock,
				sortOrder:'asc',sortName:'stockCode',fit:true" >
	        <thead>
	                <th data-options="field:'stockCode',width:100,sortable:true" editor="{type:'validatebox',options:{required:true}}">证券代码</th>
	                <th data-options="field:'stockName',width:100" editor="{type:'validatebox',options:{required:true}}">证券名称</th>
	                <th data-options="field:'stockType',width:100,flexset:'SD_STOCK_TYPE'">证券种类</th>
	            </tr>
	        </thead>
	    </table>
    </div>
    <div data-options="region:'center'">
	    <table id="dg2" class='easyui-edatagrid' style="width:100%"
			data-options="region:'center',rownumbers:true,singleSelect:true,pagination:true,method:'post',
				url : plat.fullUrl('/stock/stockprice/pagequery.do'),
				sortOrder:'desc',sortName:'reDate',fit:true">
	        <thead>
	            <tr>
	                <th data-options="field:'stockCode',width:100">证券代码</th>
	                <th data-options="field:'reDate',width:100">日期</th>
	                <th data-options="field:'priceClose',width:100">收盘价</th>
	                <th data-options="field:'volume',width:100">成交量</th>
	            </tr>
	        </thead>
	    </table>
	</div>
</body>
</html>
